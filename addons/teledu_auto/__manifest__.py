# -*- coding: utf-8 -*-
{
    'name' : 'teledu auto',
    'version' : '15.0',
    'summary': 'Auto install module',
    'description': """
    """,
    'category': 'teledu/auto',
    'website': 'https://teledu.telesoho.com',
    'depends' : ['web'],
    'license': 'LGPL-3',
    'installable': True,
    'auto_install': True,
    'data': [
        'data/config_paramenter.xml',
        'data/webclient_templates.xml',
    ],
}
