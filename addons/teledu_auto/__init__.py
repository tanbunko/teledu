from . import model 

from odoo.http import WebRequest, request, Response
from werkzeug import urls, utils
import logging

_log = logging.getLogger(__name__)

# def redirect(self, location, code=303, local=True):
#     # compatibility, Werkzeug support URL as location
#     if isinstance(location, urls.URL):
#         location = location.to_url()
#     if local:
#         ICP = self.env['ir.config_parameter'].sudo()
#         if ICP.get_param('web.base.url.redirect'):        
#             location = urls.url_parse(location).replace(scheme='', netloc='').to_url()
#             base_url = ICP.get_param('web.base.url')
#             location = f"{base_url}{location}" 
#     _log.info(f"location={location}")
#     if request and request.db:
#         return request.registry['ir.http']._redirect(location, code)
#     return utils.redirect(location, code, Response=Response)

# WebRequest.redirect = redirect

super_werkzeug_redirect = utils.redirect

def werkzeug_redirect(location, code=302, Response=None):
    if isinstance(location, urls.URL):
        location = location.to_url()
    _log.info(f"location={location}")
    ICP = request.env['ir.config_parameter'].sudo()
    if ICP.get_param('web.base.url.redirect') == 'True':        
        location = urls.url_parse(location).replace(scheme='', netloc='').to_url()
        base_url = ICP.get_param('web.base.url')
        location = f"{base_url}{location}" 

    _log.info(f"location={location}")

    return super_werkzeug_redirect(location, code, Response)

utils.redirect = werkzeug_redirect 
