#!/bin/bash
log_src='['${0##*/}']'

RED='\033[0;31m'
NC='\033[0m' # No Color


echo -e $log_src[`date +%F.%H:%M:%S`]"$RED Call common startup ... $NC"
startup_common


# load addon from git server, must lock check
# lock_file=/opt/odoo/custom/teledu.lock
# :>>$lock_file
# {
#     flock 100
#     if [ ! -d /opt/odoo/custom/teledu ]; then
#         echo -e $log_src[`date +%F.%H:%M:%S`]"$RED Clone teledu 15.0 from gitlab... $NC"
#         sudo -i -u odoo git clone --single-branch --branch 15.0 git@gitlab.com:tanbunko/teledu.git /opt/odoo/custom/teledu
#     fi
# } 100<$lock_file


if [ -f /opt/scripts/init/init ]; then
    echo -e $log_src[`date +%F.%H:%M:%S`]"$RED Call init ... $NC"
    chmod +x /opt/scripts/init/init 
    /opt/scripts/init/init
fi
